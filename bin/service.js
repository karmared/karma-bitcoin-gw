const config = require(__dirname + '/../config/config.json');

const ApiServer = require(__dirname + '/../src/api');
const Exchange = require('../src/bitshares');
const Bitcoin = require('../src/bitcoin');
const BigNumber = require('bignumber.js');

const models = require('../models');
const Op = models.Sequelize.Op;

const logger = require('../src/logger')(require('path').basename(__filename));

logger.info('Started');

var lastBlock = null;
var lastOp = null;
let exchange = null;
let bitcoin = null;

var tokens = config.tokens;
var exchangeTokens = {};

for (let name in tokens) {
    if (tokens.hasOwnProperty(name)) {
        exchangeTokens[tokens[name].exchangeId] = name;
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const startApiServer = async function () {
    apiServer = new ApiServer();
    apiServer.setBitcoin(bitcoin);
    apiServer.setExchange(exchange);
    apiServer.start();
    console.log('API server started');
};

const processWithdraw = async function (payment) {
    console.log(`New withdraw to ${payment.address} (txid: ${payment.id})`);

    let exchangeId = payment.asset;
    if (!exchangeTokens.hasOwnProperty(exchangeId)) {
        logger.error(`Withdrawing asset ${exchangeId} is not supported`);
        return;
    }
    let token = tokens[exchangeTokens[exchangeId]];
    let amount = new BigNumber(payment.amount);
    amount = amount.shiftedBy(-token.exchangePrecision);


    let record = null;
    let created = null;
    try {
        let status = 'NEW';
        if (amount.isLessThan(token.min)) {
            logger.error(`Withdraw less than min: ${payment.id}`);
            status = 'SMALL';
        }

        [record, created] = await models.Withdraws.findOrCreate({
            where: {txId: payment.id}, defaults: {
                txId: payment.id,
                from: payment.from,
                to: payment.to,
                amount: payment.amount,
                asset: payment.asset,
                memo: payment.memo,
                blockNumber: payment.blockNumber,
                status: status,
                raw: JSON.stringify(payment.raw)
            }
        });
    } catch (e) {
        logger.error(`Cannot save withdraw op #${payment.id}: ${e}`);
        process.exit(1);
    }

    if (record.status !== "NEW")
        return;

    if (!created) {
        logger.error(`Withdraws record already in DB. Check it manually, id #${record.id}`);
        return;
    }

    // Take fee
    amount = amount.minus(token.fee).shiftedBy(token.precision);

    let withdrawId = null;
    try {
        // Validate address
        let address = payment.memo;
        if (!bitcoin.isValidAddress(address))
            throw new Error('Incorrect address format');

        withdrawId = await bitcoin.send(address, amount.toFixed(0), payment.id);

        if (!withdrawId)
            throw new Error('No tx details');
    } catch (e) {
        logger.error(`Withdraw transfer failed, op #${record.id}: ${e}`);
        record.error = e;
    }

    if (!record.error) {
        logger.info(`Withdraw op #${record.id} successfully processed: ${JSON.stringify(withdrawId)}`);
    } else {
        logger.warn(`Error happened while processing withdraw op #${record.id}`);
        return;
    }

    try {
        await record.update({status: 'DONE', withdrawId: JSON.stringify(withdrawId)});
    } catch (e) {
        logger.error(`Cannot update record #${record.id}`);
        process.exit(1);
    }

    // reserve extra tokens
    //await exchange.checkHotBalance(exchangeId);
};

async function processDeposit(payment) {
    console.log(`New deposit to ${payment.to} (txid: ${payment.txId})`);

    let token = tokens[payment.asset];
    let amount = new BigNumber(payment.amount);
    amount = amount.shiftedBy(-token.precision);

    let record = null;
    let created = null;
    try {

        let status = 'NEW';
        if (amount.isLessThan(tokens[payment.asset].min)) {
            logger.error(`Deposit less than min: ${payment.txId}`);
            status = 'SMALL';
        }

        [record, created] = await models.Deposits.findOrCreate({
            where: {txId: payment.txId}, defaults: {
                txId: payment.txId,
                to: payment.to,
                amount: payment.amount,
                asset: payment.asset,
                blockNumber: payment.blockNumber,
                raw: JSON.stringify(payment.raw)
            }
        });

        if (status !== 'NEW') {
            return;
        }

    } catch (e) {
        logger.error(`Cannot save deposit op #${payment.txId}: ${e}`);
        process.exit(1);
    }

    if (!created) {
        logger.error(`Deposit record already in DB. Check it manually, id #${record.id}`);
        return;
    }

    let address = await models.Addresses.findOne({where: {address: payment.to}});
    if (!address) {
        logger.error(`Unknown deposit address, ${record.id}`);
    }

    let depositId = null;
    try {
        let assetId = token.exchangeId;

        let depositAmount = amount.shiftedBy(token.exchangePrecision);
        let accountId = '1.2.' + String(address.accountId);

        depositId = await exchange.transfer(accountId, depositAmount.toFixed(0), assetId);
        if (!depositId)
            throw new Error('No tx details');
    } catch (e) {
        logger.error(`Deposit transfer failed, op #${record.id}: ${e}`);
        record.depositError = e;
    }

    if (!record.depositError) {
        logger.info(`Deposit op #${record.id} successfully processed: ${JSON.stringify(depositId)}`);
    } else {
        logger.warn(`Error happened while processing deposit op #${record.id}`);
        return;
    }

    try {
        await record.update({depositStatus: 'DONE', depositId: JSON.stringify(depositId)});
    } catch (e) {
        logger.error(`Cannot update record #${record.id}`);
        process.exit(1);
    }
}

const saveLastBlock = async function (block) {
    await lastBlock.update({value: block});
};

const saveLastOp = async function (op) {
    await lastOp.update({value: op});
};

// Loading current state
async function main() {

    exchange = new Exchange(config.exchange);

    bitcoin = new Bitcoin();
    bitcoin.setConfig(config.bitcoin);

    try {
        await exchange.connect();
    } catch (e) {
        logger.error('Cannot connect to gate');
        process.exit(1);
    }

    try {
        await bitcoin.connect();
    } catch (e) {
        logger.error('Cannot connect to node');
        process.exit(1);
    }

    // Monitoring

    // Bitcoin
    try {
        [lastBlock] = await models.States.findOrCreate({where: {name: 'last_block'}});
    } catch (e) {
        logger.error('Cannot findOrCreate last_block property');
        process.exit(1);
    }

    if (!lastBlock.value) {
        lastBlock.update({value: await bitcoin.getCurrentBlock()});
    }

    bitcoin.on('newIn', (payment) => processDeposit(payment));
    bitcoin.on('lastBlock', (block) => saveLastBlock(block));
    bitcoin.startMonitoring(lastBlock.value);

    // Exchange
    try {
        [lastOp] = await models.States.findOrCreate({where: {name: 'last_operation'}});
    } catch (e) {
        logger.error('Cannot findOrCreate last_operation property');
        process.exit(1);
    }

    if (!lastOp.value) {
        lastOp.update({value: await exchange.getLastOperation()})
    }

    exchange.on('newIn', (payment) => processWithdraw(payment));
    exchange.on('lastOperation', (op) => saveLastOp(op));
    exchange.start(lastOp.value);

    startApiServer();
}


main();
