const jayson = require('jayson/promise');
const config = require(__dirname + '/../config/config.json');
const _ = require('lodash');
const logger = require('../src/logger')(require('path').basename(__filename));
const BigNumber = require('bignumber.js');

const models = require('../models');
const Op = models.Sequelize.Op;


class ApiServer {
    constructor() {
        this._bitcoin = null;
        this._exchange = null;

        this.server = jayson.server({

            generate_address: (args) => { return this.generateAddress(args) },
            check_address: (args) => { return this.checkAddress(args) },
            info: (args) => { return this.info(args) },
            balance: (args) => { return this.balance(args) },
            is_exist: (args) => { return this.isExist(args) },
            rejection: function (args) {
                return new Promise(function (resolve, reject) {
                    // server.error just returns {code: 501, message: 'not implemented'}
                    reject(server.error(501, 'not implemented'));
                });
            }
        });
    }

    checkAddress(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 2) {
                reject({code: 403, message: 'bad params'});
            }

            let type = args[0];
            if (type !== 'BTC') {
                reject({code: 403, message: 'unsupported type'});
                return
            }

            let address = args[1];

            let isValid = self._bitcoin.isValidAddress(address);
            console.log(`Address ${address} is ${isValid ? "valid" : "invalid"}`);
            return resolve({isValid: isValid});
        })
    }

    setBitcoin(bitcoin) {
        this._bitcoin = bitcoin;
    }

    setExchange(exchange) {
        this._exchange = exchange;
    }

    generateAddress(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 2) {
                reject({code: 403, message: 'bad params'});
            }

            let accountId = null;
            try {
                let type = args[0];
                if (type !== 'BTC') {
                    reject({code: 403, message: 'unsupported type'});
                    return
                }


                let name = args[1];
                let id = await self._exchange.getAccountId(name);
                accountId = Number(id);
                if (isNaN(accountId) || accountId === 0) throw new Error('NaN');
            } catch (e) {
                return reject({code: 403, message: 'invalid account'});
            }

            return models.Addresses.findOne({where: {accountId: accountId}}).then(record => {
                if (record) {
                    return resolve({address: record.address});
                }

                return self._bitcoin.getDepositAddress(accountId).then(address => {
                    return models.Addresses.create({
                        accountId: accountId,
                        address: address
                    }).then((record) => {
                        return resolve({address: record.address});
                    });
                });

            }).catch(e => {
                logger.error('Error gen address:', e);
                return reject({code: 500, message: 'server error'});
            });
        })
    }

    isExist(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 2) {
                reject({code: 403, message: 'bad params'});
            }

            let accountId = null;
            try {
                let type = args[0];
                if (type !== 'BTC') {
                    reject({code: 403, message: 'unsupported type'});
                    return
                }


                let name = args[1];
                let id = await self._exchange.getAccountId(name);
                accountId = Number(id);
                if (isNaN(accountId) || accountId === 0) throw new Error('NaN');
            } catch (e) {
                return reject({code: 403, message: 'invalid account'});
            }

            return models.Addresses.findOne({where: {accountId: accountId}}).then(record => {
                return resolve({"exists": record ? true : false});
            }).catch(e => {
                logger.error('Error gen address:', e);
                return reject({code: 500, message: 'server error'});
            });
        })
    }

    balance(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 1) {
                reject({code: 403, message: 'bad params'});
            }

            let balance = null;
            try {
                let type = args[0];
                if (type !== 'BTC') {
                    reject({code: 403, message: 'unsupported type'});
                    return
                }
                balance = await self._bitcoin.getBalance();
            } catch (e) {
                return reject({code: 500, message: 'server error'});
            }

            return resolve({"balance": balance});
        })
    }

    info(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 1) {
                reject({code: 403, message: 'bad params'});
            }

            let type = args[0];
            if (type !== 'BTC') {
                let type = args[0];
                reject({code: 403, message: 'unsupported type'});
                return
            }

            resolve({type: "BTC", min: config.tokens["BTC"].min, fee: config.tokens["BTC"].fee});
        })
    }

    start() {
        this.server.http().listen(config.port);
    }
}

module.exports = ApiServer;