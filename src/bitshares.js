const {Apis, ChainConfig} = require("karmajs-ws");
const {ChainStore, FetchChain, PrivateKey, TransactionHelper, Aes, TransactionBuilder} = require("karmajs");

const logger = require('../src/logger')(require('path').basename(__filename));

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

ChainConfig.expire_in_secs = 300;


const TYPES = {
    TRANSFER: 0,
};

const HOT_BALANCE = 1 *10000000;

class Bitshares {

    constructor(config) {

        this._address = config.api;
        this._accountId = config.accountId;
        this._memoKey = PrivateKey.fromWif(config.memoKey);
        this._activeKey = PrivateKey.fromWif(config.activeKey);

        this._lastOperation = null;
        this._historyLimit = 100; // 1<limit<=100

        this._dbApi = null;
        this._historyApi = null;

        this._handlers = {
            'newIn': [],
            'lastOperation': []
        };

        this._operationsStack = [];
    }

    on(eventName, callback) {
        if (this._handlers.hasOwnProperty(eventName)) {
            this._handlers[eventName].push(callback);
        }
    }

    off(eventName, handle) {
        if (this._handlers.hasOwnProperty(eventName)) {
            let handleIndex = this._handlers[eventName].indexOf(handle);
            if (handleIndex >= 0) {
                this._handlers[eventName].splice(handleIndex, 1);
            }
        }
    }

    emit() {
        let p = [];
        let args = Array.prototype.slice.call(arguments);
        let eventName = args.shift();
        if (this._handlers.hasOwnProperty(eventName)) {
            for (let i = 0, length = this._handlers[eventName].length; i < length; i++)
                p.push(this._handlers[eventName][i].apply(undefined, args));
        }
        return p;
    }

    decryptMemo(memo) {
        return Aes.decrypt_with_checksum(this._memoKey,
            memo.from,
            memo.nonce,
            memo.message).toString();
    }

    async getBalance(asset) {
        let account = await FetchChain('getAccount', this._accountId);
        let balanceId = null;
        let balances = account.get('balances');
        if (balances.has(asset)) {
            balanceId = balances.get(asset);
        }
        if (!balanceId) return 0;
        let balance = await FetchChain('getObject', balanceId);
        return balance.get('balance');
    }

    async checkHotBalance(asset) {
        let balance = await this.getBalance(asset);
        if (balance > HOT_BALANCE*2) {
            await this.reserveAsset(balance - HOT_BALANCE, asset);
        }
    }

    async transfer(to, amount, asset, memo) {
        logger.verbose(to, amount, asset);

        let balance = await this.getBalance(asset);
        if (balance < Number(amount)) {
            // let issueAmount = Number(amount) - balance + HOT_BALANCE;
            // await this.issueAsset(issueAmount, asset);

            console.error("Not enough BTC");
            process.exit(1);
        }

        let toAccount = await FetchChain("getAccount", to);

        let memoToKey = toAccount.getIn(["options", "memo_key"]);
        let nonce = TransactionHelper.unique_nonce_uint64();

        let tx = new TransactionBuilder();
        let op = {
            fee: {
                amount: 0,
                asset_id: '1.3.0'
            },
            from: this._accountId,
            to: toAccount.get("id"),
            amount: {amount: amount, asset_id: asset}
        };
        if (memo) {
            op.memo = {
                from: this._memoKey.toPublicKey().toPublicKeyString(),
                to: memoToKey,
                nonce,
                message: Aes.encrypt_with_checksum(
                    this._memoKey,
                    memoToKey,
                    nonce,
                    memo
                )
            };
        }
        tx.add_type_operation("transfer", op);
        return await this.processTransaction(tx);
    }

    async issueAsset(amount, asset) {
        let tx = new TransactionBuilder();
        tx.add_type_operation("asset_issue", {
            fee: {
                amount: 0,
                asset_id: 0
            },
            issuer: this._accountId,
            asset_to_issue: {
                amount: amount,
                asset_id: asset
            },
            issue_to_account: this._accountId,
        });
        return await this.processTransaction(tx);
    }

    async reserveAsset(amount, asset) {
        let tx = new TransactionBuilder();
        tx.add_type_operation("asset_reserve", {
            fee: {
                amount: 0,
                asset_id: 0
            },
            amount_to_reserve: {
                amount: amount,
                asset_id: asset
            },
            payer: this._accountId,
            extensions: [
            ]
        });
        return await this.processTransaction(tx);
    }

    async processTransaction(tx) {
        await tx.set_required_fees();
        tx.add_signer(this._activeKey, this._activeKey.toPublicKey().toPublicKeyString());
        let x = await tx.broadcast();
        return x;
    }

    async getLastOperation() {

        let result = await this._historyApi.exec("get_account_history", [this._accountId,
            '1.11.0', 1, '1.11.0']);

        return result[0].id;
    }

    async getAccountId(name) {
        let account = await FetchChain('getAccount', name);
        let id = Number(account.get('id').replace('1.2.',''));
        return id;
    }

    async getTxId(item) {

        let r = await this._dbApi.exec('get_block', [item.block_num]);
        let tx = r.transactions[item.trx_in_block];
        let txBuffer = ops.transaction.toBuffer(tx);

        return hash.sha256(txBuffer).toString('hex').substring(0, 40);
    }

    async getNewOperations() {

        let mostRecentOpId = null;
        let dynGlobalProperties = await this._dbApi.exec("get_dynamic_global_properties", []);
        let lastIrreversibleBlockNum = dynGlobalProperties.last_irreversible_block_num;
        //let lastIrreversibleBlockNum = dynGlobalProperties.head_block_number;

        let opId = "1.11.0";
        let previousOpId = null;

        while (true) {

            // if requesting the same
            if (previousOpId == opId)
                break;
            previousOpId = opId;

            let result = await this._historyApi.exec("get_account_history", [this._accountId,
                this._lastOperation,
                this._historyLimit,
                opId]);
            logger.verbose(`Getting account history, from=${this._lastOperation}, to=${opId}, limit=${this._historyLimit}, result=${result.length}`);

            // no new records
            if (result.length === 0)  break;

            let stop = false;
            for (let i = 0; i < result.length; i++) {
                let item = result[i];

                // ignore processed items
                if (opId == item.id) continue;
                opId = item.id;

                // Ignore very fresh blocks
                if (item.block_num > lastIrreversibleBlockNum) {
                    continue;
                }

                if (mostRecentOpId === null) mostRecentOpId = item.id;

                let opType = item.op[0];
                let opData = item.op[1];

                logger.verbose(`Got new operation: ${opId}, ${opType}`);

                if (opId === this._lastOperation || opType === 5) {
                    stop = true;
                    break;
                }

                this._operationsStack.push({
                    id: opId,
                    type: opType,
                    data: opData,
                    blockNumber: item.block_num
                });
            }
            if (stop) break;
        }
    }

    async startBlockchainScan() {
        while (true) {
            await this.getNewOperations();

            let op = this._operationsStack.pop();
            while (op) {

                if (op.type === TYPES.TRANSFER &&
                    op.data.from !== this._accountId &&
                    op.data.to === this._accountId) {

                    // decrypting memo
                    let memo = null;
                    let error = null;
                    try {
                        if (op.data.hasOwnProperty('memo'))
                            memo = this.decryptMemo(op.data.memo);
                    } catch (e) {
                        logger.error('Cannot decrypt memo from operation #', op.id, ': ', e);
                        error = 'Cannot decrypt memo';
                    }

                    await Promise.all(this.emit('newIn', {
                        id: op.id,
                        txId: op.txId,
                        blockNumber: op.blockNumber,
                        from: op.data.from,
                        to: op.data.to,
                        memo: memo,
                        amount: op.data.amount.amount,
                        asset: op.data.amount.asset_id,
                        raw: op,
                        error: error
                    }));
                }

                this._lastOperation = op.id;
                await Promise.all(this.emit('lastOperation', this._lastOperation));

                op = this._operationsStack.pop();
            }
            this._operationsStack = [];

            await sleep(10 * 1000);
        }
    }

    async start(lastOperation) {
        logger.verbose('Starting scan from', lastOperation);

        this._lastOperation = lastOperation;
        if (!this._lastOperation) this._lastOperation = '1.11.0';

        return this.startBlockchainScan();
    }

    async connect() {
        let res = await Apis.instance(this._address, true).init_promise;
        logger.info('Connected to:', res[0].network.chain_id);
        await ChainStore.init();

        this._dbApi = Apis.instance().db_api();
        this._historyApi = Apis.instance().history_api();
    }
}

module.exports = Bitshares;
