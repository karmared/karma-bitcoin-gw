const logger = require('../src/logger')(require('path').basename(__filename));
const BigNumber = require('bignumber.js');
const Bluebird = require('bluebird');

const Client = require('bitcoin-core');
var bip39 = require('bip39');
var bitcoinjs = require('bitcoinjs-lib');

var WAValidator = require('wallet-address-validator');


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

class Wallet {

    constructor() {

        this._wallet = {
            main: null,
            deposit: null
        };

        this._rootKey = null;
        this._isMainnet = false;

        this._confirmations = 6;

        this._bitcoin = null;

        this._stopMonitoring = null;

        this._handlers = {
            'newIn': [],
            'lastBlock': []
        };
    }

    on(eventName, callback) {
        if (this._handlers.hasOwnProperty(eventName)) {
            this._handlers[eventName].push(callback);
        }
    }

    off(eventName, handle) {
        if (this._handlers.hasOwnProperty(eventName)) {
            let handleIndex = this._handlers[eventName].indexOf(handle);
            if (handleIndex >= 0) {
                this._handlers[eventName].splice(handleIndex, 1);
            }
        }
    }

    emit() {
        let p = [];
        let args = Array.prototype.slice.call(arguments);
        let eventName = args.shift();
        if (this._handlers.hasOwnProperty(eventName)) {
            for (let i = 0, length = this._handlers[eventName].length; i < length; i++)
                p.push(this._handlers[eventName][i].apply(undefined, args));
        }
        return p;
    }

    async connect() {
        try {
            logger.info('connected', this._host);
        } catch (e) {
            logger.error(`Cannot connect to ${this._host}: ${e}`);
            throw e;
        }
    }

    setConfig(config) {
        this._bitcoin = new Client({
            ssl: config.ssl,
            host: config.host,
            port: config.port,
            username: config.username,
            network: config.network,
            password: config.password
        });

        if (!config.mnemonic) throw new Error("No mnemonic seed");
        const seed = bip39.mnemonicToSeed(config.mnemonic);
        this._rootKey = bitcoinjs.HDNode.fromSeedBuffer(seed);

        this._isMainnet = config.network === "mainnet";

        if (config.confirmations && Number(config.confirmations) > 0)
            this._confirmations = Number(config.confirmations);
    }

    async getDepositAddress(account) {

        const path = `m/44'/0'/${account}'/0/0`;
        const child = this._rootKey.derivePath(path);

        const privateKey = child.keyPair.toWIF();
        const address = child.keyPair.getAddress().toString();
        console.log('Account:', account);
        console.log('Generated address:', address);
        await this._bitcoin.importPrivKey(privateKey, String(account), false);
        const accountFromWallet = await this._bitcoin.getAccount(address);

        if (accountFromWallet !== String(account))
            throw new Error("Error while generating address");
        return address;
    }

    async getCurrentBlock() {
        let currentBlock = await this._bitcoin.getBlockCount();
        return currentBlock;
    }

    async getBalance() {
        let balance = await this._bitcoin.getBalance();
        return balance;
    }

    async startMonitoring(start) {

        this._stopMonitoring = false;

        let _start = start ? Number(start) : 0;
        let _startHash = await this._bitcoin.getBlockHash(_start);
        console.log("Start from block:", _start, _startHash);

        while (true) {
            try {
                logger.info('From:', _start, _startHash);
                let data = await this._bitcoin.listSinceBlock(_startHash, this._confirmations);
                console.log("D:", data.transactions.length);

                for (let j = 0; j < data.transactions.length; j++) {
                    let tx = data.transactions[j];

                    // BTC
                    if (tx.account && tx.category === "receive" && tx.confirmations >= this._confirmations) {

                        let blockInfo = await this._bitcoin.getBlock(tx.blockhash);
                        let amount = new BigNumber(tx.amount.toString());

                        let payment = {
                            txId: tx.txid,
                            blockNumber: blockInfo.height,
                            from: null,
                            to: tx.address,
                            amount: amount.shiftedBy(8).toFixed(),
                            asset: 'BTC',
                            raw: tx,
                            error: null
                        };

                        await Promise.all(this.emit('newIn', payment));
                    }
                }

                let blockInfo = await this._bitcoin.getBlock(data.lastblock);

                if (blockInfo.height) {
                    await Promise.all(this.emit('lastBlock', blockInfo.height));
                    _start = blockInfo.height;
                    _startHash = data.lastblock;
                }
            } catch (e) {
                logger.error('Error proccess transactions:', e);
            }

            if (this._stopMonitoring) break;

            await sleep(15 * 1000);
        }
    }

    async send(address, amount, comment = null) {
        try {
            let _amount = new BigNumber(amount).shiftedBy(-8);
            let txid = await this._bitcoin.sendToAddress(address, _amount.toNumber(), comment);
            return txid;
        } catch (e) {
            console.error("Cannot send BTC:", e);
            throw e;
        }
    }

    stopMonitoring() {
        this._stopMonitoring = true;
    }

    isValidAddress(address) {
        let res = WAValidator.validate(address, 'bitcoin', this._isMainnet ? "mainnet" : "testnet");
        return res;
    }
}

module.exports = Wallet;
