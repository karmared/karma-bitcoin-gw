'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('deposits', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            to: {
                type: Sequelize.STRING,
                allowNull: false
            },
            amount: {
                type: Sequelize.BIGINT,
                allowNull: false
            },
            asset: {
                field: 'asset',
                allowNull: false,
                type: Sequelize.STRING
            },
            blockNumber: {
                field: 'block_number',
                allowNull: false,
                type: Sequelize.BIGINT
            },
            txId: {
                field: 'tx_id',
                allowNull: false,
                type: Sequelize.STRING
            },
            raw: {
                field: 'raw',
                allowNull: false,
                type: Sequelize.TEXT
            },
            status: {
                field: 'status',
                allowNull: false,
                type: Sequelize.STRING,
                defaultValue: 'NEW'
            },
            error: {
                field: 'error',
                type: Sequelize.STRING,
            },
            depositStatus: {
                field: 'deposit_status',
                allowNull: false,
                type: Sequelize.STRING,
                defaultValue: 'NEW'
            },
            depositError: {
                field: 'error',
                type: Sequelize.STRING,
            },
            depositId: {
                field: 'deposit_id',
                type: Sequelize.STRING,
            },
            createdAt: {
                field: 'created_at',
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                field: 'updated_at',
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('deposits');
    }
};