'use strict';
module.exports = (sequelize, DataTypes) => {
    var States = sequelize.define('States', {
        name: {
            unique: true,
            primaryKey: true,
            allowNull: false,
            type: DataTypes.STRING
        },
        value: {
            allowNull: true,
            type: DataTypes.STRING
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'states'
    });
    States.associate = function (models) {
        // associations can be defined here
    };
    return States;
};