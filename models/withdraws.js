'use strict';
module.exports = (sequelize, DataTypes) => {
    var Withdraws = sequelize.define('Withdraws', {
        from: {
            type: DataTypes.STRING,
            allowNull: false
        },
        to: {
            type: DataTypes.STRING,
            allowNull: false
        },
        amount: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        asset: {
            type: DataTypes.STRING,
            allowNull: false
        },
        memo: {
            field: 'memo',
            allowNull: true,
            type: DataTypes.STRING
        },
        blockNumber: {
            field: 'block_number',
            type: DataTypes.BIGINT,
            allowNull: false
        },
        status: {
            field: 'status',
            allowNull: false,
            type: DataTypes.STRING,
            defaultValue: 'NEW'
        },
        error: {
            field: 'error',
            type: DataTypes.STRING,
        },
        withdrawId: {
            field: 'withdraw_id',
            type: DataTypes.STRING
        },
        txId: {
            field: 'tx_id',
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        raw: {
            allowNull: false,
            type: DataTypes.TEXT
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'withdraws'
    });
    Withdraws.associate = function (models) {
        // associations can be defined here
    };
    return Withdraws;
};