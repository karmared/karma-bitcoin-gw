'use strict';
module.exports = (sequelize, DataTypes) => {
    var Deposits = sequelize.define('Deposits', {
        to: {
            type: DataTypes.STRING,
            allowNull: false
        },
        amount: {
            type: DataTypes.BIGINT,
            allowNull: false
        },
        asset: {
            type: DataTypes.STRING,
            allowNull: false
        },
        blockNumber: {
            field: 'block_number',
            type: DataTypes.BIGINT,
            allowNull: false
        },
        status: {
            field: 'status',
            allowNull: false,
            type: DataTypes.STRING,
            defaultValue: 'NEW'
        },
        error: {
            field: 'error',
            type: DataTypes.STRING,
        },
        depositStatus: {
            field: 'deposit_status',
            allowNull: false,
            type: DataTypes.STRING,
            defaultValue: 'NEW'
        },
        depositError: {
            field: 'error',
            type: DataTypes.STRING,
        },
        depositId: {
            field: 'deposit_id',
            type: DataTypes.STRING,
        },
        txId: {
            field: 'tx_id',
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        raw: {
            allowNull: false,
            type: DataTypes.TEXT
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'deposits'
    });
    Deposits.associate = function (models) {
        // associations can be defined here
    };
    return Deposits;
};