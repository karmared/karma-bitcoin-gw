'use strict';
module.exports = (sequelize, DataTypes) => {
    var PaymentRequests = sequelize.define('PaymentRequests', {
        requestId: {
            type: DataTypes.STRING,
            field: 'request_id',
            unique: true,
            allowNull: false
        },
        to: {
            type: DataTypes.STRING,
            allowNull: false
        },
        amount: {
            type:DataTypes.STRING,
            allowNull: false
        },
        asset: {
            type:DataTypes.STRING,
            allowNull: false
        },
        status: {
            type:DataTypes.STRING,
            allowNull: false,
            defaultValue: 'NEW'
        },
        error: {
            type:DataTypes.TEXT,
        },
        txId: {
            field: 'tx_id',
            type:DataTypes.STRING,
        },
        txRaw: {
            field: 'tx_raw',
            type:DataTypes.TEXT,
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'payment_requests'
    });
    PaymentRequests.associate = function (models) {
        // associations can be defined here
    };
    return PaymentRequests;
};